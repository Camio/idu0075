/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolapp;

import ee.ttu.ws.schoolservice.*;
import ee.ttu.ws.schoolservice.StudyPlan;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;

/**
 * REST Web Service
 *
 * @author Home
 */
@Path("study-plans")
public class StudyPlansResource {

    @Context
    private UriInfo context;

    public StudyPlansResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public StudyPlanList getStudyPlans(
            @NotNull @QueryParam("token") String token,
            @NotNull @QueryParam("student") String student
    ) {
        SchoolService ws = new SchoolService();
        GetStudyPlanListRequest request = new GetStudyPlanListRequest();
        request.setToken(token); 
        request.setStudent(student);
        return ws.getStudyPlanList(request);
    }

    @GET
    @Path("{id: [0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public StudyPlan getStudyPlan(
            @NotNull @QueryParam("token") String token, 
            @NotNull @PathParam("id") BigInteger id) {
        SchoolService ws = new SchoolService();
        GetStudyPlanRequest request = new GetStudyPlanRequest();
        request.setToken(token);
        request.setId(id);        
        return ws.getStudyPlan(request);
    }
        
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addStudyPlan(
            @NotNull @QueryParam("token") String token,
            @NotNull @QueryParam("student") String student) {
        try {
            SchoolService ws = new SchoolService();
            AddStudyPlanRequest request = new AddStudyPlanRequest();
            request.setToken(token);
            request.setStudent(student);
            StudyPlan response = ws.addStudyPlan(request);
            return Response.status(Response.Status.CREATED).type(MediaType.APPLICATION_JSON).entity(response).build();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(StudyPlansResource.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }
    
    @GET
    @Path("{id: [0-9]*}/courses")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Course> getCoursesStudyPlan(
            @NotNull @QueryParam("token") String token, 
            @NotNull @PathParam("id") BigInteger id) {
        SchoolService ws = new SchoolService();
        GetCourseStudyPlanRequest request = new GetCourseStudyPlanRequest();
        request.setToken(token);
        request.setStudyPlanId(id);        
        return ws.getCourseStudyPlan(request).getCourse();
    }
    
    @POST
    @Path("{id: [0-9]*}/courses")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCourseStudyPlan(
            @NotNull @QueryParam("token") String token, 
            @NotNull @QueryParam("course-code") String code,
            @NotNull @PathParam("id") BigInteger id) {
        SchoolService ws = new SchoolService();
        AddCourseStudyPlanRequest request = new AddCourseStudyPlanRequest();
        request.setToken(token);
        request.setStudyPlanId(id);
        request.setCourseCode(code);
        AddCourseStudyPlanResponse response = ws.addCourseStudyPlan(request);
        switch(response.getMessage()){
            case "FAILED: Unable to find study plan":
            case "FAILED: Unable to find course":
                return Response.status(Response.Status.PRECONDITION_FAILED).type(MediaType.APPLICATION_JSON).entity(response.getMessage()).build();
            case "Course is already part of study plan.":
                return Response.status(Response.Status.fromStatusCode(409)).type(MediaType.APPLICATION_JSON).entity(response.getStudyPlan()).build();
            case "Added course to study plan":
                return Response.status(Response.Status.fromStatusCode(201)).type(MediaType.APPLICATION_JSON).entity(response.getStudyPlan()).build();
            default:
                return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
