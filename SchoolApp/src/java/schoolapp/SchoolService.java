/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolapp;

import java.util.List;
import javax.jws.WebService;
import ee.ttu.ws.schoolservice.*;
import java.math.BigInteger;
import java.util.ArrayList;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.ws.rs.NotAuthorizedException;


/**
 *
 * @author Jekaterina Vassiljeva
 */
@WebService(serviceName = "SchoolService", portName = "SchoolPort", endpointInterface = "ee.ttu.ws.schoolservice.SchoolPortType", targetNamespace = "http://www.ttu.ee/ws/SchoolService", wsdlLocation = "WEB-INF/wsdl/SchoolService/schoolService.wsdl")
public class SchoolService {
    private static final CourseList coursesList = new CourseList();
    private static final StudyPlanList studyPlanList = new StudyPlanList();
    
    public AddCourseResponse addCourse(AddCourseRequest parameter) throws NotAuthorizedException {
        Authorize.validateToken(parameter.getToken());
        AddCourseResponse response = new AddCourseResponse();
        List<Course> courses =  coursesList.getCourse();
        if(_getCourseByCode(courses, parameter.getCourse().getCode()) == null){
            coursesList.getCourse().add(parameter.getCourse());
            response.setCourse(parameter.getCourse());
            response.setMessage("OK");
        } else {
            response.setMessage("FAILED: Course with same code already exists");
        } 
        return response;               
    }

    public Course getCourse(GetCourseRequest parameter) {
        Authorize.validateToken(parameter.getToken());
        List<Course> courses =  coursesList.getCourse();
        String code = parameter.getCode();
        return _getCourseByCode(courses, code);
    }
    
    public CourseList getCourseList(GetCourseListRequest parameter) {
        Authorize.validateToken(parameter.getToken());
        List<Course> courses =  coursesList.getCourse();
        if(parameter.getCode() != null){
            courses = _getCoursesByCode(courses, parameter.getCode());
        }
        if(parameter.getTitle()!= null){
            courses = _getCoursesByTitle(courses, parameter.getTitle());
        }
        if(parameter.getSemester()!= null && parameter.getSemester() != SemesterSelection.ANY){
            courses = _getCoursesBySemester(courses, parameter.getSemester());
        }
        CourseList filteredCoursesList = new CourseList();
        for(Course course : courses) {
            filteredCoursesList.getCourse().add(course);
        }
        return filteredCoursesList;
    }

    public StudyPlan addStudyPlan(AddStudyPlanRequest parameter) throws DatatypeConfigurationException {
        Authorize.validateToken(parameter.getToken());
        StudyPlan studyPlan  = new StudyPlan();
        studyPlan.setStudent(parameter.getStudent());
        studyPlan.setId(BigInteger.valueOf(studyPlanList.getStudyPlans().size()));
        studyPlan.setSubmissionDate(Calenader.currentDateTime());
        studyPlan.setCourses(new CourseList());
        studyPlanList.getStudyPlans().add(studyPlan);
        return studyPlan;
    }
    
    public StudyPlan getStudyPlan(GetStudyPlanRequest parameter) {
        Authorize.validateToken(parameter.getToken());
        return _getStudyPlanById(parameter.getId());
    }

    public StudyPlanList getStudyPlanList(GetStudyPlanListRequest parameter) {
        Authorize.validateToken(parameter.getToken());
        StudyPlanList filteredList= new StudyPlanList();
        for(StudyPlan studyPlan : studyPlanList.getStudyPlans()){
            if(studyPlan.getStudent().contains(parameter.getStudent())){
                filteredList.getStudyPlans().add(studyPlan);
            }
        }
        return filteredList;
    }

    public AddCourseStudyPlanResponse addCourseStudyPlan(AddCourseStudyPlanRequest parameter) {
        Authorize.validateToken(parameter.getToken());
        AddCourseStudyPlanResponse response = new AddCourseStudyPlanResponse();
        StudyPlan studyPlan = _getStudyPlanById(parameter.getStudyPlanId());
        if (studyPlan == null){
            response.setMessage("FAILED: Unable to find study plan");
            return response;
        }
        
        List<Course> allCourses =  coursesList.getCourse();
        Course course = _getCourseByCode(allCourses, parameter.getCourseCode());
        if (course == null){
            response.setMessage("FAILED: Unable to find course");
            return response;
        }
        
        List<Course> studyPlanCourses = studyPlan.getCourses().getCourse();
        Course studyPlanCourse = _getCourseByCode(studyPlanCourses, parameter.getCourseCode());
        if (studyPlanCourse != null){
                response.setMessage("Course is already part of study plan.");
        } else {
            studyPlanCourses.add(course);
            studyPlan.setTotalPrice(studyPlan.getTotalPrice() + course.getPrice());
            response.setMessage("Added course to study plan");
        }
        
        response.setStudyPlan(studyPlan);
        
        return response;
    }

    public CourseList getCourseStudyPlan(GetCourseStudyPlanRequest parameter) {
        Authorize.validateToken(parameter.getToken());
        StudyPlan studyPlan = _getStudyPlanById(parameter.getStudyPlanId());
        return studyPlan.getCourses();
    }
        
    private Course _getCourseByCode(List<Course> courses, String code){
        for (Course course : courses){
            String courseCode = course.getCode();
            if(courseCode == null ? code == null : courseCode.equals(code)){
                return course;
            }
        }
        return null;
    }
    
    private StudyPlan _getStudyPlanById(BigInteger id){
        List<StudyPlan> studyPlans = studyPlanList.getStudyPlans();
        for(StudyPlan studyPlan : studyPlans){
            if(studyPlan.getId().equals(id)){
                return studyPlan;
            }
        }
        return null;
    }

    private List<Course> _getCoursesByCode(List<Course> courses, String code) {
        List<Course> filteredCourses = new ArrayList<>();
        for (Course course : courses){
            if(course.getCode().contains(code)){
                filteredCourses.add(course);
            }
        }
        return filteredCourses;
    }
    
    private List<Course> _getCoursesByTitle(List<Course> courses, String title) {
        List<Course> filteredCourses = new ArrayList<>();
        for (Course course : courses){
            if(course.getTitle().contains(title)){
                filteredCourses.add(course);
            }
        }
        return filteredCourses;
    }
    
    private List<Course> _getCoursesBySemester(List<Course> courses, SemesterSelection semester) {
        List<Course> filteredCourses = new ArrayList<>();
        for (Course course : courses){
            SemesterSelection courseSemester = course.getSemester();
            if(courseSemester == semester || courseSemester == SemesterSelection.ANY){
                filteredCourses.add(course);
            }
        }
        return filteredCourses;
    }
}
