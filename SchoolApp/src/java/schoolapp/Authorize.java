/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolapp;

import java.util.Arrays;
import java.util.List;
import javax.ws.rs.NotAuthorizedException;

/**
 *
 * @author Jekaterina Vassiljeva
 */
public class Authorize {
    private static final List<String> TOKENS = Arrays.asList("FJ9NVg8NMWvr2hVT", "LJ6zf7euKemyeqtW","ZxJSUpa3kFH5wcR2");

    static void validateToken(String token) {
        for(String validToken : TOKENS){
            if (validToken == null ? token == null : validToken.equals(token)){
                return;
            }
        }
        throw new NotAuthorizedException("Authorization failed");
    }
    
}
