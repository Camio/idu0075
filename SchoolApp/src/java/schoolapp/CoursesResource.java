/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolapp;

import ee.ttu.ws.schoolservice.CourseList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import ee.ttu.ws.schoolservice.*;
import javax.validation.constraints.NotNull;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Jekaterina Vassiljeva
 */
@Path("courses")
public class CoursesResource {

    @Context
    private UriInfo context;

    public CoursesResource() {
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public CourseList getCourses(
            @NotNull @QueryParam("token") String token,
            @QueryParam("code") String code,
            @QueryParam("title") String title,
            @QueryParam("semester") SemesterSelection semester
            ) {
        SchoolService ws = new SchoolService();
        GetCourseListRequest request = new GetCourseListRequest();
        request.setToken(token);
        request.setCode(code);
        request.setTitle(title);
        request.setSemester(semester);       
        return ws.getCourseList(request);
    }
    
    @GET
    @Path("{code}")
    @Produces({MediaType.APPLICATION_JSON})
    public Course getCourse(@NotNull @QueryParam("token") String token, 
            @NotNull @PathParam("code") String code) {
        SchoolService ws = new SchoolService();
        GetCourseRequest request = new GetCourseRequest();
        request.setToken(token);
        request.setCode(code);        
        return ws.getCourse(request);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCourse(
            @NotNull @QueryParam("token") String token,
            @NotNull Course course) {
        SchoolService ws = new SchoolService();
        AddCourseRequest request = new AddCourseRequest();
        request.setToken(token);
        request.setCourse(course);
        AddCourseResponse response = ws.addCourse(request);
        switch(response.getMessage()){
            case "OK":
                return Response.status(Response.Status.CREATED).type(MediaType.APPLICATION_JSON).entity(response.getCourse()).build();
            case "FAILED: Course with same code already exists":
                return Response.status(Response.Status.fromStatusCode(409)).type(MediaType.APPLICATION_JSON).entity(response).build();
            default:
                return Response.status(Response.Status.BAD_REQUEST).build(); 
        }
    }
}
