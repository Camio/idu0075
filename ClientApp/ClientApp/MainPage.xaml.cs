﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using ClientApp.SchoolService;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ClientApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private const string token = "ZxJSUpa3kFH5wcR2";

        private async void AddCourse_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var checkedButton =
                    CourseSemesterPanel.Children.OfType<RadioButton>().FirstOrDefault(r => r.IsChecked == true);
                var client = new SchoolPortTypeClient();
                var course = new course
                {
                    code = CourseCode.Text,
                    assessmentForm = CourseAssessmentForm.Text,
                    title = CourseTitle.Text,
                    lecturer = CourseLecturer.Text,
                    semester =
                        (semesterSelection)
                        Enum.Parse(typeof(semesterSelection), checkedButton.Content?.ToString(), true),
                    price = Convert.ToDouble(CoursePrice.Text)
                };
                var request = new addCourseRequest
                {
                    token = token,
                    course = course
                };
                var response = await client.addCourseAsync(new addCourseRequest1(request));
                ResponseText.Text = response.addCourseResponse.message;
            }
            catch (Exception ex)
            {
                ResponseText.Text = "Unable to add course.\nError: " + ex.Message;
            }
        }

        private async void GetCoursesButton_OnClick(object sender, RoutedEventArgs e)
        {
            var checkedButton =
                    CourseSemesterSearchPanel.Children.OfType<RadioButton>().FirstOrDefault(r => r.IsChecked == true);
            var client = new SchoolPortTypeClient();
            var request = new getCourseListRequest {
                token = token,
                code = CourseCodeSearch.Text,
                title = CourseTitleSearch.Text,
                semesterSpecified = checkedButton != null,
                semester = (semesterSelection)
                        Enum.Parse(typeof(semesterSelection), checkedButton?.Content?.ToString(), true)
            };
            var response = await client.getCourseListAsync(new getCourseListRequest1(request));
            CourseList.ItemsSource = response.getCourseListResponse1;
        }

        private async void GetCourseByCode_OnClick(object sender, RoutedEventArgs e)
        {
            var client = new SchoolPortTypeClient();
            var request = new getCourseRequest
            {
                token = token,
                code = CourseFullCodeSearch.Text
            };
            var response = await client.getCourseAsync(new getCourseRequest1(request));
            if (response.getCourseResponse1.code == null)
            {
                FoundCourse.ItemsSource = null;
                GetCourseByCodeResponseText.Text = "No results found";
            }
            else
            {
                var list = new List<course> {response.getCourseResponse1};
                FoundCourse.ItemsSource = list;
            }
        }
    }
}
